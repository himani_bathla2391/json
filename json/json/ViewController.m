//
//  ViewController.m
//  json
//
//  Created by Clicklabs 104 on 10/29/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lonlabel;
@property (weak, nonatomic) IBOutlet UILabel *latlabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionlabel;
@property (weak, nonatomic) IBOutlet UILabel *tempminlabel;
@property (weak, nonatomic) IBOutlet UILabel *tempmaxlabel;
@property (weak, nonatomic) IBOutlet UILabel *pressurelabel;
@property (weak, nonatomic) IBOutlet UIImageView *image;

@end

@implementation ViewController
@synthesize lonlabel;
@synthesize latlabel;
@synthesize descriptionlabel;
@synthesize tempminlabel;
@synthesize tempmaxlabel;
@synthesize pressurelabel;
@synthesize image;
int i;
-(void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *filepath= [[NSBundle mainBundle] pathForResource:@"File" ofType:@"json"];
    NSData *data= [NSData dataWithContentsOfFile:filepath];
    NSDictionary *master = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    NSDictionary *code= master[@"coord"];
    
    NSString *longi= code[@"lon"];
    lonlabel.text= [NSString stringWithFormat: @"%@",longi ];
    NSString *lat= code[@"lat"];
    latlabel.text= [NSString stringWithFormat: @"%@",lat ];
    
    NSArray *weather =master[@"weather"];
    for (i=0;i<weather.count;i++ ){
    NSString *description= weather[i][@"description"];
    NSLog(@"%@",description);
    descriptionlabel.text= [NSString stringWithFormat: @"%@",description ];}
    
    NSDictionary *main= master[@"main"];
    
    NSString *pressure= main[@"pressure"];
    pressurelabel.text= [NSString stringWithFormat:@"%@",pressure];
    NSString *tempmin= main[@"temp_min"];
    tempminlabel.text= [NSString stringWithFormat:@"%@",tempmin];
    NSString *tempmax= main[@"temp_max"];
    tempmaxlabel.text= [NSString stringWithFormat:@"%@",tempmax];
  
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
